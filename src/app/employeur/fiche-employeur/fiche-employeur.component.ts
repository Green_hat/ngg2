import { Component, OnInit } from '@angular/core';
import {Employeur, Regime} from '../../Employeur';
import {EmployeurService} from '../employeur.service';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-fiche-employeur',
  templateUrl: './fiche-employeur.component.html',
  styleUrls: ['./fiche-employeur.component.css']
})
export class FicheEmployeurComponent implements OnInit {
  empl: Employeur;

  constructor( public emplService: EmployeurService) { }

  ngOnInit() {
    this.empl = this.emplService.getEmpl();

  }

}
