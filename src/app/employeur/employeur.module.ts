import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FicheEmployeurComponent} from './fiche-employeur/fiche-employeur.component';
import {EmployeurService} from './employeur.service';

@NgModule({
  declarations: [    FicheEmployeurComponent],
  imports: [
    CommonModule
  ],
  providers: [EmployeurService]
})
export class EmployeurModule { }
