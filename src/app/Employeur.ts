export enum Regime { General = 0, Agricole = 1 }
export class Employeur {
    constructor(private _matricule, private _cle,
                private _raisonSociale, private _regime: Regime) { }

    get matricule() {
        return this._matricule;
    }

    set matricule(value) {
        this._matricule = value;
    }

    get cle() {
        return this._cle;
    }

    set cle(value) {
        this._cle = value;
    }

    get raisonSociale() {
        return this._raisonSociale;
    }

    set raisonSociale(value) {
        this._raisonSociale = value;
    }

    get regime(): Regime {
        return this._regime;
    }

    set regime(value: Regime) {
        this._regime = value;
    }
}
