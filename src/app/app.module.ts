// https://bitbucket.org/Green_hat/ngg2



import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AssureModule} from './assure/assure.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AssureModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
