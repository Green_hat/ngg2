import { Component } from '@angular/core';
import {Employeur, Regime} from './Employeur';

@Component({
  selector: 'app-empl',
  template: `
  <div >
    <h1>
      Welcome to gestion {{ title }}!
    </h1>
  </div>
  <div>
    <app-list-assure></app-list-assure>
  </div>
  <router-outlet></router-outlet>
  `,
  styles: ['h1{    color: blueviolet;}']
})
export class AppComponent {
  title = 'employeur';

}
