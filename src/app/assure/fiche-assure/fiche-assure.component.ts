import {Component, Input, OnInit} from '@angular/core';
import {AssureService} from '../assure.service';
import {Assure} from '../assure';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-fiche-assure',
  templateUrl: './fiche-assure.component.html',
  styleUrls: ['./fiche-assure.component.css']
})
export class FicheAssureComponent implements OnInit {
  @Input()
  public assure: Assure;


  constructor(private assuerService: AssureService) { }
  ngOnInit() {

  }

}
