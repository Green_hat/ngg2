import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAssureComponent } from './form-assure.component';

describe('FormAssureComponent', () => {
  let component: FormAssureComponent;
  let fixture: ComponentFixture<FormAssureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAssureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAssureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
