import {Component, Input, OnInit} from '@angular/core';
import {Assure} from '../assure';
import {AssureService} from '../assure.service';

@Component({
  selector: 'app-form-assure',
  templateUrl: './form-assure.component.html',
  styleUrls: ['./form-assure.component.css']
})
export class FormAssureComponent implements OnInit {
  @Input()
  public assure: Assure;
  constructor(private assureService: AssureService) {
  }

  ngOnInit() {
  this.assure = new Assure();
  }

  save() {
      if (this.assure.id) {
        this.assureService.update(this.assure);
        console.log('updating' + this.assure);
      } else {
        this.assureService.create(this.assure);
        console.log('creating' + this.assure);
      }
  }

  log() {
    console.log(this.assure);
  }
}
