import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Assure} from './assure';
@Injectable()
export class AssureService {
  constructor(private http: HttpClient) { }
  findAll() {
    return this.http.get('http://localhost:8080/assure/');
  }

  find(id: number) {
    return this.http.get<Assure>('http://localhost:8080/assure/' + id);
  }

  delete(id: number) {
    this.http.delete('http://localhost:8080/assure/' + id)
      .subscribe(resp => {
      console.log(resp);
      this.findAll();
    });
  }

  create(assure: Assure) {
      this.http.post('http://localhost:8080/assure/', assure)
        .subscribe(resp => console.log(resp));
  }

  update(assure: Assure) {
    this.http.put('http://localhost:8080/assure/' + assure.id, assure)
      .subscribe(resp => console.log(resp));
  }
}

