import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AppComponent} from '../app.component';
import {FicheAssureComponent} from './fiche-assure/fiche-assure.component';

import {HttpClient, HttpClientModule} from '@angular/common/http';
import {ListAssureComponent} from './list-assure/list-assure.component';
import {AssureService} from './assure.service';
import {FormsModule} from '@angular/forms';
import {FormAssureComponent} from './form-assure/form-assure.component';

@NgModule({
  declarations: [
    FicheAssureComponent,
    ListAssureComponent,
    FormAssureComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    HttpClient,
    AssureService
  ],
  exports: [
    FicheAssureComponent,
    ListAssureComponent
  ]
})
export class AssureModule { }
