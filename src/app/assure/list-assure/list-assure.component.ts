import { Component, OnInit } from '@angular/core';
import {AssureService} from '../assure.service';
import {Assure} from '../assure';

@Component({
  selector: 'app-list-assure',
  templateUrl: './list-assure.component.html',
  styleUrls: ['./list-assure.component.css']
})
export class ListAssureComponent implements OnInit {
  public listAssure;
  public hide = true;
  public assure;
  constructor(private assureService: AssureService) { }
  ngOnInit() {

    this.assureService.findAll()
      .subscribe(assures => this.listAssure = assures);
  }
  afficherAssure(id) {
    this.hide = false;
    this.assureService.find(id).subscribe(response => this.assure = response);

  }

  showtoggle() {
    this.hide = !this.hide;
  }

  delete(id: number) {
    this.assureService.delete(id);
  }

  populate(assure: Assure) {

  }
}
